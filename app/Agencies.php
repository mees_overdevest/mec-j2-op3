<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agencies extends Model
{
    protected $fillable = [
        'created_at',
        'updated_at',
        'title',
        'description'
    ];

    protected $dates = ['created_at','updated_at'];

    public function findings()
    {
        return $this->belongsToMany('App\Findings', 'agencies_findings','agencies_id','findings_id')
            ->withPivot('title','description')
            ->withTimestamps();
    }


}
