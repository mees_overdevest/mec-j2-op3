<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Agencies;

class Findings extends Model
{
    //
    protected $fillable = [
        'title',
        'description',
        'category_id',
        'location',
        'created_at',
        'updated_at',
        'published_at'
    ];

    protected $dates = ['created_at','updated_at'];

    public function category()
    {
        return $this->belongsTo('App\Categories');
    }


    public function agencies()
    {
        return $this->belongsToMany('App\Agencies');
    }
}
