<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('findings');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

// Een resource route, die verwijst naar alle controlleracties
Route::resource('categories', 'CategoriesController');
// Een resource route, die verwijst naar alle controlleracties
Route::resource('findings', 'FindingsController');
// Een resource route, die verwijst naar alle controlleracties
Route::resource('agencies', 'AgenciesController');

// Een route, die verwijst naar de overzichtspagina van de app
Route::get('test','AnalyticsController@test');

// Een route, die een export maakt van alle bevindingen in de database
Route::get('export','AnalyticsController@export');

// Twee ajax routes, die verwijzen naar de verschillende functionele resources
Route::get('getFindings','AnalyticsController@getFindings');
Route::get('getAgencies','AnalyticsController@getAgencies');

//Route::get('agencies', 'AgenciesController@index');
//Route::get('agencies/create', 'AgenciesController@create');
//Route::post('agencies', 'AgenciesController@store');

Route::group(['middleware' => ['web']], function () {
    //
});
