<?php

namespace App\Http\Controllers;

use App\Agencies;
use Illuminate\Http\Request;

use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Findings;
use Excel;
use App\Categories;

class AnalyticsController extends Controller
{
    public function test()
    {
        $findings = Findings::all();

        return view('analytics.test',compact('findings'));
    }

    public function getFindings()
    {
        $findings = Findings::with('agencies','category')->get();

        return response()->json(['response' => $findings ]);
    }

    public function getAgencies(){
        $agencies = Agencies::all();
        $findings = Findings::with('agencies','category')->get();

        return response()->json(['response' => $findings, 'agencies' => $agencies]);
    }

    public function export(){
        $findings = Findings::all();

        Excel::create('Bevindingen overzicht', function($excel) use($findings){

            $excel->sheet('Bevindingen overzicht', function($sheet) use ($findings){
               $sheet->loadview('findings.export.sheet')->with('findings', $findings);
            });
//            foreach($findings as $finding){
//                $
//            }

        })->download('xls');

    }
}
