<?php

namespace App\Http\Controllers;

use App\Findings;
use App\Categories;
use App\Agencies;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FindingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $findings = Findings::all();


        return view('findings.index', compact('findings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agencies = Agencies::all();
        $categories = Categories::lists('title','id');

        return view('findings.create', compact('agencies', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        $finding = Findings::create($request->all());
        $agencies = $request->input('agency');

        $finding->title = $request->get('title');
        $finding->description = $request->get('description');
        $finding->location = $request->get('location');
        $finding->category_id = $request->get('category_id');

        $published = $request->get('published_at');

//        $published = strtotime(date('d-m-Y H:i', str_replace('/','-',$published)));

        $finding->published_at = $published;

        $finding->save();

//        dd($agencies);

        foreach($agencies as $key => $value)
        {
            if($value == "on")
            {
                $finding->agencies()->attach($key);
            }
        }

        return redirect('findings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $finding = Findings::findOrFail($id);

        return view('findings.show', compact('finding'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $finding = Findings::findOrFail($id);
        $categories = Categories::lists('title','id');
        $agencies = Agencies::all();

        $findingAgencies = [];

        foreach($finding->agencies as $agency){
            $findingAgencies[] = $agency->title;
        }


        return view('findings.edit', compact('finding','categories','agencies','findingAgencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $finding = Findings::findOrFail($id);

        $agencies = $request->input('agency');
        $finding->updated_at = Carbon::now();
        $finding->description = $request->get('description');
        $finding->title = $request->get('title');
        $finding->category_id = $request->get('category_id');

        $published = $request->get('published_at');


//        $published = strtotime(str_replace('/','-',$published));
//
//        dd($published);
//            Carbon::parse($published)->format('d/m/Y');
//

        $finding->published_at = $published;

        $finding->save();
        $syncthis = [];
        //dd($agencies);
        foreach($agencies as $key => $value) {
            if ($value == "on") {
                $syncthis[] .= $key;
            } else {
                $finding->agencies()->detach([$key]);
            }

        }

        $finding->agencies()->sync($syncthis);

//        if($agencies){
//            foreach($agencies as $key => $value){
//                if($value == "on")
//                {
//                    $finding->agencies()->sync([$key]);
//                } else {
//                    $finding->agencies()->detach([$key]);
//                }
//            }
//
//        }

        //$finding->update($request->all());

        return redirect('findings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
