<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = [
        'title',
        'description',
        'created_at',
        'updated_at'
    ];

    protected $dates = ['created_at','updated_at'];

    /**
     *  A Category can have many Findings
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function findings()
    {
        return $this->HasMany('App\Findings');
    }
}
