<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Tracking App</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categories <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ action('CategoriesController@index') }}">All Categories</a></li>
                        <li><a href="{{ action('CategoriesController@create') }}">New Category</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Agencies <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ action('AgenciesController@index') }}">All Agencies</a></li>
                        <li><a href="{{ action('AgenciesController@create') }}">New Agency</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Findings <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ action('FindingsController@index') }}">All Findings</a></li>
                        <li><a href="{{ action('FindingsController@create') }}">New Finding</a></li>
                    </ul>
                </li>
            {{--</ul>--}}
            {{--<ul class="nav navbar-nav navbar-right">--}}
                {{--<li>--}}
                    {{--<a>--}}
                        {{--@if(Auth::user())--}}
                            {{--Hello, {{ Auth::user()->firstName }} {{ Auth::user()->lastName }}--}}
                        {{--@endif--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li><a href="{{ action('Auth\AuthController@login') }}">Login</a></li>--}}
                {{--<li><a href="{{ action('Auth\AuthController@register') }}">Register</a></li>--}}
                {{--<li><a href="{{ action('Auth\AuthController@logout') }}">Log out</a></li>--}}
            {{--</ul>--}}
        </div><!--/.nav-collapse -->
    </div>
</nav>