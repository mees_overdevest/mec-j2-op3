@extends('app')

@section('content')
    <h1>Create a new Finding</h1>
    <hr/>

    {!! Form::open(['url' => 'findings']) !!}
    @include('findings._form', ['submitButtonText'=>'Create Finding'])
    {!! Form::close() !!}



@stop



@section('footer')
    <script type="text/javascript">
        $(function(){
            $('#published_at').datetimepicker({
                theme:'dark',
                showSecond: true,
                timeFormat: 'hh:mm',
                yearStart: 2015,
                yearEnd: 2017
//                changeMonth: true,
//                change
            });
        });
    </script>
@stop