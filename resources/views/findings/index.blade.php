@extends('app')

@section('content')
    <h1>Findings</h1>
    <a href="{{ action('FindingsController@create') }}" class="btn btn-success">New Finding</a>
    <hr/>
    @foreach ($findings as $finding)
        <article>
            <h2>
                <a href="{{ action('FindingsController@show', [$finding->id]) }}">{{ $finding->id }}. {{ $finding->title }}</a>
            </h2>
            <a href='{{ action('FindingsController@edit', [$finding->id]) }}' class="btn btn-success">Edit Finding</a>

            <div class="body">
                <h3>Category: {{ $finding->category['title'] }}</h3>
                <h3>Agencies:</h3>
                <ul>
                    @foreach($finding->agencies as $agency)
                        <li>{{ $agency->title }}</li>
                    @endforeach
                </ul>
                <p>Beschrijving: {{ $finding->description }}</p>
                <p>Tijd: {{ $finding->published_at }}</p>
                <p>Tijd: {{ $finding->created_at }}</p>
            </div>

        </article>
    @endforeach

@stop



@section('footer')

@stop