<html>
<table>
    <tr>
        <th>Bevindingsnummer</th>
        <th>Datum</th>
        <th>Locatie</th>
        <th>Titel</th>
        <th>Beschrijving</th>
        <th>Categorie</th>
        <th>Bedrijven</th>
    </tr>

    @foreach($findings as $finding)

        <tr class="payments">
            <td>{{ $finding->id }}</td>
            <td>{{ $finding->published_at }}</td>
            <td>
                {{ $finding->location }}
            </td>
            <td>{{ $finding->title }}</td>
            <td>{{ $finding->description }}</td>
            <td>{{ $finding->category->title }}</td>
            <td>
                @foreach($finding->agencies as $agency)
                    {{ $agency->title }},
                @endforeach
            </td>
        </tr>
    @endforeach
</table>
</html>