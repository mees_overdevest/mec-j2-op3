@extends('app')

@section('content')
    <h1>{{ $finding->title }}</h1>
    <hr/>

    <article>
        {{ $finding->description}}
    </article>

    {{ $finding->created_at }}

@stop
