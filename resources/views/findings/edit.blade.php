@extends('app')

@section('content')
    <h1>Edit: {!! $finding->title !!}</h1>

    <table>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Location</th>
            <th>Category</th>
            <th>Agencies</th>
        </tr>
        <tr>
            <td>{{ $finding->title }}</td>
            <td>{{ $finding->description }}</td>
            <td>{{ $finding->location }}</td>
            <td>{{ $finding->category->title }}</td>
            <td>
                <ul>
                    @foreach($finding->agencies as $agency)
                        <li>{{ $agency->title }}</li>
                    @endforeach
                </ul>
            </td>
        </tr>
    </table>

    {!! Form::model($finding, ['method' => 'PATCH', 'action' => ['FindingsController@update', $finding->id]]) !!}
    <div class="form-group">
        {!! Form::label('title', 'Finding Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Finding Description:') !!}
        {!! Form::text('description', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('location', 'Finding Location:') !!}
        {!! Form::text('location', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('category_id', 'Finding Category:') !!}
        {!! Form::select('category_id',$categories,$finding->category->id,['class' => 'form-control'])!!}
    </div>

    <div class="form-group">
        {!! Form::label('published_at', 'Published At:') !!}
        {!! Form::date('published_at', null,['class' => 'form-control']) !!}
    </div>

    @foreach($agencies as $agency)

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <label for="{{ $agency->id }}">{{ $agency->title }}</label>
            <input type="checkbox" name="agency[{{ $agency->id }}]" id="{{ $agency->id }}" class="form-control"

            @if(in_array($agency->title, $findingAgencies))
                checked="true"
           @endif
            >
        </div>
    @endforeach

    <div class="form-group">
        {!! Form::submit('Update Finding', ['class' => 'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}

@stop

@section('footer')
    <script type="text/javascript">
        $(function(){
            jQuery('#published_at').datetimepicker({
                theme:'dark',
                showSecond: true,
                timeFormat: 'hh:mm',
                yearStart: 2015,
                yearEnd: 2017
//                changeMonth: true,
//                change
            });
        });
    </script>
@stop