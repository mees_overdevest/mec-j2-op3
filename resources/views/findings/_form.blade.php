<div class="form-group">
    {!! Form::label('title', 'Finding Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description', 'Finding Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('location', 'Finding Location:') !!}
    {!! Form::text('location', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('category_id', 'Finding Category:') !!}
    {!! Form::select('category_id',$categories,['class' => 'form-control'])!!}
</div>

<div class="form-group">
    {!! Form::label('published_at', 'Published At:') !!}
    {!! Form::date('published_at', '',['class' => 'form-control']) !!}
</div>

@foreach($agencies as $agency)

    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <label for="{{ $agency->id }}">{{ $agency->title }}</label>
        <input type="checkbox" name="agency[{{ $agency->id }}]" id="{{ $agency->id }}" class="form-control">
    </div>
@endforeach

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>