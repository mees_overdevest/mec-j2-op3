@extends('app')

@section('content')
    <h1>Agencies</h1>
    <a href="{{ action('AgenciesController@create') }}" class="btn btn-success">New Agency</a>
    <hr/>
    @foreach ($agencies as $agency)
        <article>
            <h2>
                <a href="{{ action('AgenciesController@show', [$agency->id]) }}">{{ $agency->title }}</a>
            </h2>
            <a href='{{ action('AgenciesController@edit', [$agency->id]) }}' class="btn btn-success">Edit Agency</a>

            <div class="body">Beschrijving: {{ $agency->description }}</div>
        </article>
    @endforeach

@stop



@section('footer')

@stop