@extends('app')

@section('content')
    <h1>Create a new Agency</h1>
    <hr/>

    {!! Form::open(['url' => 'agencies']) !!}
    @include('agencies._form', ['submitButtonText'=>'Create Agency'])
    {!! Form::close() !!}



@stop



@section('footer')

@stop