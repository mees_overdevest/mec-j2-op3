@extends('app')

@section('content')
    <h1>{{ $agency->title }}</h1>
    <hr/>

    <article>
        {{ $agency->description}}
    </article>

@stop
