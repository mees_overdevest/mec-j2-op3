@extends('app')

@section('content')
    <h1>Edit: {!! $agency->title !!}</h1>

    {!! Form::model($agency, ['method' => 'PATCH', 'action' => ['AgenciesController@update', $agency->id]]) !!}
        @include('agencies._form', ['submitButtonText'=>'Update Agency'])
    {!! Form::close() !!}

@stop