@extends('app')

@section('content')
    <div id="chart"></div>
@stop

@section('footer')
    <script type="text/javascript">
//        var svg = dimple.newSvg("body", 800, 600);
//        var data = [
//            { "Word":"Hello", "Awesomeness":2000 },
//            { "Word":"World", "Awesomeness":3000 }
//        ];
//        var chart = new dimple.chart(svg, data);
//        chart.addCategoryAxis("x", "Word");
//        chart.addMeasureAxis("y", "Awesomeness");
//        chart.addSeries(null, dimple.plot.bar);
//        chart.draw();

        var svg = dimple.newSvg("#chart", 590, 400);
        var data = [
            { "Brand":"Supercoolio", "Sales Volume":1000, "Sales Value":1500, "Price":1.5 },
            { "Brand":"Coolio", "Sales Volume":800, "Sales Value":1300, "Price":0.9 },
            { "Brand":"Averagium", "Sales Volume":376, "Sales Value":800, "Price":0.8 },
            { "Brand":"Uncoolio", "Sales Volume":250, "Sales Value":750, "Price":0.5 },
            { "Brand":"Uncoolio Max", "Sales Volume":120, "Sales Value":100, "Price":0.2 },

        ];
        var myChart = new dimple.chart(svg, data);
        myChart.addMeasureAxis("x", "Sales Volume");
        myChart.addMeasureAxis("y", "Price");
        myChart.addMeasureAxis("z", "Sales Value");
        myChart.addSeries("Brand", dimple.plot.bubble);
        myChart.draw();
    </script>
@stop