@extends('app')

@section('content')
    <h1>Categories</h1>
    <a href="{{ action('CategoriesController@create') }}" class="btn btn-success">New Category</a>
    <hr/>
    @foreach ($categories as $category)
        <article>
            <h2>
                <a href="{{ action('CategoriesController@show', [$category->id]) }}">{{ $category->title }}</a>
            </h2>
            <a href='{{ action('CategoriesController@edit', [$category->id]) }}' class="btn btn-success">Edit Category</a>

            <div class="body">Beschrijving: {{ $category->description }}</div>
        </article>
    @endforeach

@stop



@section('footer')

@stop