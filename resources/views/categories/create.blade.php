@extends('app')

@section('content')
    <h1>Create a new Category</h1>
    <hr/>

    {!! Form::open(['url' => 'categories']) !!}
    @include('categories._form', ['submitButtonText'=>'Create Category'])
    {!! Form::close() !!}



@stop



@section('footer')

@stop