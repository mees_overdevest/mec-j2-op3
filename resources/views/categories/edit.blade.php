@extends('app')

@section('content')
    <h1>Edit: {!! $category->title !!}</h1>

    {!! Form::model($category, ['method' => 'PATCH', 'action' => ['CategoriesController@update', $category->id]]) !!}
        @include('categories._form', ['submitButtonText'=>'Update Category'])
    {!! Form::close() !!}

@stop