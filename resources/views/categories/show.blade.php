@extends('app')

@section('content')
    <h1>{{ $category->title }}</h1>
    <hr/>

    <article>
        {{ $category->description}}
    </article>

@stop
