@extends('app')

@section('content')
    <h1>Analyse your findings!</h1>
    <div id="chart"></div>
    <p>Click on one of the buttons below to analyse those findings.</p>
    <div class="btn-group">
        <button id="categories" type="button" class="btn btn-default">Get finding categories</button>
        <button id="agencies" type="button" class="btn btn-default">Get finding agencies</button>
    </div>
@stop

@section('footer')
    <script src="{{ Request::root()}}/js/analytics.js"></script>
@stop