$("button").click(function() {
    var chartData = [];
    $("#chart").empty();

    if(this.id == 'categories'){
        $.get('/getFindings', function(data) {

            var findings = data.response;

            var svg = dimple.newSvg("#chart", 590, 400);

            $.each(findings, function(index, content){

                var now = {};

                var Length = content['agencies'].length;

                now.title = content['title'];
                now.category =  content['category'].title;
                now.agencies = Length;

                chartData.push(now);
            });

            var myChart = new dimple.chart(svg, chartData);
            myChart.setBounds(60, 30, 510, 305);
            var x = myChart.addCategoryAxis("x", "category");
            x.addOrderRule("desc");
            myChart.addMeasureAxis("y", "agencies");
            myChart.addSeries("title", dimple.plot.bar);
//                myChart.addLegend(200, 10, 380, 20, "right");
            myChart.draw();
        });
    }

    if(this.id == 'agencies'){
        $.get('/getAgencies', function(data) {

            var agencies = data.agencies;
            var findings = data.response;

            var svg = dimple.newSvg("#chart", 590, 400);

            $.each(findings, function(index, content){
                for(var i = 0; i < agencies.length; i++){
                    $.each(content['agencies'], function(index, value){
                        var now = {};
                        if(i == value['id'])
                        {
                            now.title = content['title'];
                            now.agency = value['title'];
                            now.category =  content['category'].title;
                            now.findings = 1;

                            chartData.push(now);
                        } else{
                            now = 0;
                        }
                    });
                }
            });

            var myChart = new dimple.chart(svg, chartData);
            myChart.setBounds(60, 30, 510, 305);
            var x = myChart.addCategoryAxis("x", "agency");
            x.addOrderRule("desc");
            myChart.addMeasureAxis("y", "findings");
            myChart.addSeries("title", dimple.plot.bar);

            myChart.draw();
        });
    }
});