<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');

            $table->timestamps();
        });

        Schema::create('agencies_findings', function (Blueprint $table)
        {
            $table->increments('id');

            $table->integer('agencies_id')->unsigned()->index();
            $table->foreign('agencies_id')->references('id')->on('agencies');

            $table->integer('findings_id')->unsigned()->index();
            $table->foreign('findings_id')->references('id')->on('findings')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agencies');
        Schema::drop('agencies_findings');
    }
}
